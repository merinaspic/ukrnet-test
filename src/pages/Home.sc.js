import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 800px;
  justify-content: center;
  margin: 0 auto;
  padding: 50px 40px;
  overflow: hidden;
`;

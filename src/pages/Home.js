import React from 'react';
import Form from '../components/form';
import CarInfo from '../components/car-info';
import { Wrapper } from './Home.sc';


export default function HomePage() {
  return (
    <Wrapper>
      <Form />
      <CarInfo />
    </Wrapper>
  );
};

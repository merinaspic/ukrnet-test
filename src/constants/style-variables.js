export const colors = {
  headerBg: '#3E51B5',
  mainBg: '#ffffff',
  mainText: '#141313',
  lightText: '#fafafa',
  errorText: '#ff4040',
  hintText: '#929090',
};

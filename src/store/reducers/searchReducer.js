import { START_SEARCH, FETCH_RESULT, FETCH_ERROR } from '../actions/types';

const initialState = {
  result: null,
  loading: false,
  error: null,
}

export default function githubReducer(state = initialState, { type, payload}) {
  switch (type) {
    case START_SEARCH:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_RESULT:
      return {
        ...state,
        result: payload,
        loading: false,
        error: null,
      };
    case FETCH_ERROR:
      return {
        ...state,
        result: null,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}

import axios from 'axios';
import { API_URL } from '../../constants/config';
import { START_SEARCH, FETCH_RESULT, FETCH_ERROR } from './types';


export const startSearch = () => {
  return {
    type: START_SEARCH,
  }
};

export const fetchResult = (payload) => {
  return {
    type: FETCH_RESULT,
    payload,
  }
};

export const fetchError = (payload) => {
  return {
    type: FETCH_ERROR,
    payload,
  }
};

export const fetchNewResult = (carNumber) => {
  return (dispatch) => {
    dispatch(startSearch());
    return axios.get(`${API_URL}/car-info/${carNumber}`)
      .then(response => {
        if (!!response.data.result) {
          dispatch(fetchResult(response.data.result))
        } else {
          dispatch(fetchError('Номер не найден'))
        }
      })
      .catch(error => {
        dispatch(fetchError('Номер не найден'))
      });
  };
};

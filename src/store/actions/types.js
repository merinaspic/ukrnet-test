export const START_SEARCH = 'START_SEARCH';
export const FETCH_RESULT = 'FETCH_RESULT';
export const FETCH_ERROR = 'FETCH_ERROR';

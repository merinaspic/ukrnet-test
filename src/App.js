import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import Home from './pages/Home';
import AppStyles from './App.sc';
import rootReducer from './store/reducers';

const store = createStore(rootReducer, compose(
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
));

export default function App() {
  return (
    <Provider store={store}>
      <AppStyles />
      <Home />
    </Provider>
  );
}

import styled from 'styled-components';
import { colors } from '../../constants/style-variables'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 100%;
  background: ${colors.headerBg};
  padding: 20px 30px 10px;
  box-sizing: border-box;
`;

export const Title = styled.span`
  display: inline-block;
  margin-bottom: 10px;
  font-size: 22px;
  font-weight: 700;
  color: ${colors.lightText};
`;

export const  Input = styled.input`
  display: inline-block;
  width: 100%;
  font-size: 19px;
  background: ${colors.mainBg};
  padding: 10px 20px;
  box-sizing : border-box;
`;

export const  ValidateError = styled.span`
  display: inline-block;
  width: 100%;
  min-height: 20px;
  line-height: 20px;
  vertical-align: middle;
  font-size: 15px;
  color: ${colors.errorText};
  margin-top: 5px;
`;

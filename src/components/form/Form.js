import React from 'react';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { fetchNewResult } from '../../store/actions';
import { Wrapper, Title, Input, ValidateError } from './Form.sc';

const initValues = {
  query: ''
};

function Form(props) {
  const { loading } = props.data;
  const validate = ({ query }) => {
    const errors = {};
    if (
      !!query && !/^[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}$/i.test(query.trim())
    ) {
      errors.query = 'Неправильный формат';
    }
    return errors;
  }

  const onPressEnter = (event, submit) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      submit();
    }
  }

  const onSubmit =({query}, { setSubmitting }) => {
    if(!!query){
      const { fetchResults } = props;
      fetchResults(query.trim());
    }
  }

  return (
    <Wrapper>
      <Title>Проверка авто по номерному знаку</Title>
      <Formik
        initialValues={initValues}
        validate={validate}
        onSubmit={onSubmit}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
        }) => (
          <form onSubmit={handleSubmit}>
            <Input
              type="text"
              name="query"
              placeholder="Номерной знак"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.query}
              disabled={loading}
              onKeyPress={(event) => {
                onPressEnter(event, handleSubmit)
              }}
            />
            <ValidateError>{touched.query && errors.query}</ValidateError>
          </form>
        )}
      </Formik>
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {
    data: state.carInfo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchResults: query => {
      dispatch(fetchNewResult(query));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Form);

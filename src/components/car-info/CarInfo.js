import React from 'react';
import { connect } from 'react-redux';
import { Wrapper, HintTitle, Info, Row, Column, Loader } from './CarInfo.sc';


function CarInfo(props) {
  const { result, loading, error } = props.data;
  return (
    <Wrapper>
      {loading && <Loader />}
      {!!result && !error &&(
        <Info>
          <Row>
            <Column>Владелец</Column>
            <Column right>{result.owner}</Column>
          </Row>
          <Row>
            <Column>Год производства</Column>
            <Column right>{result.year}</Column>
          </Row>
          <Row>
            <Column>Количество владельцев</Column>
            <Column right>{result.ownersCount}</Column>
          </Row>
          <Row>
            <Column>Факты ДДП</Column>
            <Column right>{result.crashesCount}</Column>
          </Row>
        </Info>
      )}
      {!result && !error && <HintTitle>Введите номерной знак автомобиля (например AA9999AI) и нажмите Enter</HintTitle>}
      {!result && !!error && <HintTitle>{error}</HintTitle>}
    </Wrapper>
  );
};

const mapStateToProps = state => {
  return {
    data: state.carInfo
  };
};

export default connect(
  mapStateToProps,
  null,
)(CarInfo);

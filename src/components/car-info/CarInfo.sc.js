import styled from 'styled-components';
import loader from '../../img/loader.svg';
import { colors } from '../../constants/style-variables'

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  width: 100%;
  min-height: 600px;
`;

export const HintTitle = styled.span`
  width: 100%;
  display: inline-block;
  font-size: 22px;
  font-weight: 700;
  color: ${colors.hintText};
  text-align: center;
  margin-top: 200px;
`;

export const Loader = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background: url(${loader}) no-repeat;
  background-size: 64px 64px;
  background-position: center center;
  background-color: rgba(255, 255, 255, 0.7);
  z-index: 2;
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  width: 90%;
  margin: 40px auto;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  border: 1px solid ${colors.mainText};
  border-bottom: none;
  padding: 10px 20px;
  &:last-child {
    border-bottom: 1px solid ${colors.mainText};
  }
`;

export const Column = styled.div`
  width: ${({ right }) => right ? '60%' : `40%`};
  font-weight: 600;
`;

